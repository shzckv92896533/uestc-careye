

include_directories(${PROJECT_SOURCE_DIR}/src/include)
message(STATUS "Load ControllCenter Lib")
set(CONTROL_CENTER_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/src/include)
set(CONTROL_CENTER_SOURCES_DIR ${PROJECT_SOURCE_DIR}/src/sources)
file(GLOB CONTROL_CENTER_HEADER_FILES
#        ${CONTROL_CENTER_INCLUDE_DIR}/*.h
#        ${CONTROL_CENTER_INCLUDE_DIR}/*.hpp
        ${CONTROL_CENTER_INCLUDE_DIR}/*/*.hpp
        ${CONTROL_CENTER_INCLUDE_DIR}/*/*.h
        )
file(GLOB CONTROL_CENTER_SOURCE_FILES
#        ${CONTROL_CENTER_SOURCES_DIR}/*.c
#        ${CONTROL_CENTER_SOURCES_DIR}/*.cpp
#        ${CONTROL_CENTER_SOURCES_DIR}/*.cxx
        ${CONTROL_CENTER_SOURCES_DIR}/*/*.c
        ${CONTROL_CENTER_SOURCES_DIR}/*/*.cpp
        ${CONTROL_CENTER_SOURCES_DIR}/*/*.cxx
        )
qt5_wrap_cpp(MOC_CONTROL_CENTER_HEADER_FILES ${CONTROL_CENTER_HEADERS_FILES})
set(CONTROL_CENTER_SOURCE
        ${CONTROL_CENTER_HEADER_FILES}
        ${CONTROL_CENTER_SOURCE_FILES}
        ${MOC_CONTROL_CENTER_HEADER_FILES}
        )
foreach(FILE ${CONTROL_CENTER_SOURCE})
    message(STATUS FIND:${FILE})
endforeach()
add_library(center_server SHARED ${CONTROL_CENTER_SOURCE})
target_link_libraries(center_server
        Qt5::Widgets Qt5::Network
        Qt5::Xml Qt5::Quick
        ${Boost_LIBRARIES}
        rc_grpc_proto
        )
qt5_use_modules(center_server Core Widgets Gui Network Xml Quick)