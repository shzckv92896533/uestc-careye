//
// Created by pulsarv on 19-5-2.
//

#include <asio/rc_tcp_server_boost.h>
#include <boost/bind.hpp>
#include <iostream>
void connect_handler(const boost::system::error_code &ec){

}
rc_tcp_server_boost::rc_tcp_server_boost(int port, boost::asio::io_service &ios):ios_(ios){
    boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string("0.0.0.0"), port);
    boost::asio::ip::tcp::acceptor acceptor_(ios,ep);
    boost::asio::ip::tcp::socket sock(ios);
    sock.async_connect(ep,connect_handler);
    ios.run();
    start_listen();
}

rc_tcp_server_boost::~rc_tcp_server_boost() {

}

void rc_tcp_server_boost::start_listen() {
    socket_ptr socket(new boost::asio::ip::tcp::socket(ios_));

}
void rc_tcp_server_boost::accept_handler(const boost::system::error_code& _ec, socket_ptr _socket) {
    // 错误码检测
    if (_ec) {
        return;
    }
    // 打印当前连接进来的客户端
    std::cout << "client: " << _socket->remote_endpoint().address() << std::endl;
    // 异步发送 "hello CSND_Ayo" 消息到客户端，发送成功后，自动调用Server::write_handler函数
    _socket->async_write_some(boost::asio::buffer("hello CSND_Ayo"),
                              boost::bind(&rc_tcp_server_boost::write_handler, this,
                                          boost::asio::placeholders::error/* 此处作为占位符 */));
    // 启动新的异步监听
    start_listen();
}
void rc_tcp_server_boost::write_handler(const boost::system::error_code& _ec) {
    std::cout << "server: send message complete." << std::endl;
}