//
// Created by Pulsar on 2021/8/24.
//

#ifndef ROBOCAR_RCTASKVARIABLE_H
#define ROBOCAR_RCTASKVARIABLE_H

#include <mutex>
#include <atomic>
#include <rc_system/data_struct.h>

namespace rccore {
    namespace task {
        namespace task_variable {
            extern std::atomic<unsigned short> TASK_NUM;//正在运行的任务数量
            extern std::atomic<bool> CONTEXT_IS_INITED;//全局上下文是否被初始化
            extern std::mutex global_mutex;//全局信号锁
            extern std::mutex system_info_mutex;//系统信息锁
            extern data_struct::SystemInfo systemInfo;//系统信息
        }
    }
}


#endif //ROBOCAR_RCTASKVARIABLE_H
