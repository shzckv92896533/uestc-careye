//
// Created by Pulsar on 2020/5/5.
//

#ifndef UESTC_CAREYE_RC_HTTPD_H
#define UESTC_CAREYE_RC_HTTPD_H

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <strings.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdint.h>
//TODO:实现一个简单的WEB服务器用来显示和控制移动



namespace rccore {
    namespace network {
        namespace Httpd {

            class HttpdServer {
            public:
                /**
                 * 打开服务器
                 * @param port
                 */
                void start_server(u_short port);

                /**
                 * 发送命令到队列
                 */
                void send_command();
            };
        }
    }
}

#endif //UESTC_CAREYE_RC_HTTPD_H
