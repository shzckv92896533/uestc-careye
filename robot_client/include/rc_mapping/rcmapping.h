//
// Created by PulsarV on 18-7-5.
//

#ifndef ROBOCAR_RCMAPPING_H
#define ROBOCAR_RCMAPPING_H

#include <rc_mapping/slam_devices.h>
#include <string>

namespace rccore {
    namespace mapping {
        class RcMapping : public SlamDevice {
        public:
            explicit RcMapping(std::shared_ptr<common::Context> pcontext);

        private:
            char *map_path{};
        };
    }
}


#endif //ROBOCAR_RCMAPPING_H
