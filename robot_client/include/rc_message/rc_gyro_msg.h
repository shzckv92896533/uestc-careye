//
// Created by Pulsar on 2020/5/30.
//

#ifndef UESTC_CAREYE_RC_GYRO_MSG_H
#define UESTC_CAREYE_RC_GYRO_MSG_H

#include <rc_system/data_struct.h>
#include <rc_message/rc_base_msg.hpp>

//这是一个比较特殊的类，因为陀螺仪的数据不一定一致抵达
namespace rccore {
    namespace message {
        class GyroMessage : public BaseMessage<data_struct::CJY901> {
        public:
            explicit GyroMessage(int max_queue_size);

            ~GyroMessage();
        };
    }
}


#endif //UESTC_CAREYE_RC_GYRO_MSG_H
