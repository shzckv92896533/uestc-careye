//
// Created by pulsarv on 21-1-29.
//

#ifndef FOLLOW_DOWN_VISION_H
#define FOLLOW_DOWN_VISION_H

#include <follow_down/config.h>
#include <follow_down/base_thread.h>
#include <opencv2/opencv.hpp>
#include <follow_down/ftimer.h>
#include <apriltag.h>

namespace follow_down {
    namespace vision {
        class VisionThread : public follow_down::thread::BaseThread {
        public:
            /**
             * 视觉处理线程
             * @param pconfig
             */
            explicit VisionThread(std::shared_ptr<follow_down::config::Config> pconfig);

            ~VisionThread();

            /**
             * 启动
             */
            void Run() override;

            /**
             * 导航处理
             * @param frame 图像矩阵
             * @param camMatrix 相机内参矩阵
             * @param distCoeff 图像畸变矩阵
             * @param pconfig 全局配置上下文
             * @param td TAG 检测器
             * @return 最大TAG坐标
             */
            cv::Point Navgation(
                    cv::Mat frame, cv::Mat camMatrix, cv::Mat distCoeff, std::shared_ptr<config::Config> pconfig,
                    apriltag_detector_t *td);

        private:
            std::shared_ptr<follow_down::timer::CTimer> pftimer;
        };
    } // namespace vision
} // namespace follow_down

#endif // FOLLOW_DOWN_VISION_H
