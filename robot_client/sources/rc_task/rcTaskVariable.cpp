//
// Created by Pulsar on 2021/8/24.
//

#include <rc_task/rcTaskVariable.h>

namespace rccore {
    namespace task {
        namespace task_variable {
            std::atomic<unsigned short> TASK_NUM = 0;
            std::atomic<bool> CONTEXT_IS_INITED = false;
            std::mutex global_mutex;
            std::mutex system_info_mutex;
            data_struct::SystemInfo systemInfo;
        }
    }
}