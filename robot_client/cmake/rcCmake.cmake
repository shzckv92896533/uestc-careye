message(STATUS "Generate Proto File")

file(GLOB RC_PROTO_FILES ${RC_RESOURCE_DIR}/*.proto)
PROTOBUF_GENERATE_CPP(RC_PROTO_SRCS RC_PROTO_HDRS ${RC_PROTO_FILES})
add_custom_target(proto_dep DEPENDS ${RC_PROTO_SRCS} ${RC_PROTO_HDRS})
add_custom_target(copy_html
        COMMAND cp -r ${RC_CLIENT_DIR}/html ${EXECUTABLE_OUTPUT_PATH}
        )
add_custom_target(copy_haarcascades
        COMMAND cp -r ${RC_CLIENT_DIR}/../haarcascades ${EXECUTABLE_OUTPUT_PATH}
        )


foreach (FILE ${RC_PROTO_FILES})
    message(STATUS "RC_PROTO_FILE: ${FILE}")
endforeach ()
foreach (FILE ${RC_PROTO_SRCS})
    message(STATUS "RC_PROTO_SRC: ${FILE}")
endforeach ()
foreach (FILE ${RC_PROTO_HDRS})
    message(STATUS "RC_PROTO_H: ${FILE}")
endforeach ()

file(GLOB RC_MODULES_SOURCES_DIR ${RC_CLIENT_SOURCES_DIR}/*)
file(GLOB RC_MODULES_HEADERS_DIR ${RC_CLIENT_INCLUDE_DIR}/*)

set(RC_SOURCES "")
set(RC_HEADERS "")

foreach (RC_MODULE ${RC_MODULES_SOURCES_DIR})
    message(STATUS "RC_MODULE :${RC_MODULE}")
    file(GLOB RC_MODULE_SOURCES ${RC_MODULE}/*.cpp)
    foreach (RC_MODULE_SOURCE_FILE ${RC_MODULE_SOURCES})
        message(STATUS "RC_MODULE_SOURCE_FILE :${RC_MODULE_SOURCE_FILE}")
        LIST(APPEND RC_SOURCES ${RC_MODULE_SOURCE_FILE})
    endforeach ()
endforeach ()

foreach (RC_MODULE ${RC_MODULES_HEADERS_DIR})
    file(GLOB RC_MODULE_HEADERS ${RC_MODULE}/*.h)
    foreach (RC_MODULE_HEADEER_FILE ${RC_MODULE_HEADERS})
        message(STATUS "RC_MODULE_HEADEER_FILE :${RC_MODULE_HEADEER_FILE}")
        LIST(APPEND RC_HEADERS ${RC_MODULE_HEADEER_FILE})
    endforeach ()
endforeach ()
LIST(LENGTH RC_HEADERS SOURCES_LENGTH)
message(STATUS "SOURCES_LENGTH:${SOURCES_LENGTH}")

set(RC_THREAD_LIBS
        pthread
        ${OPENGL_LIBRARIES}
        ${GLUT_LIBRARY}
        ${PYTHON_LIBRARIES}
        ${OpenCV_LIBS}
        ${Boost_LIBRARIES}
        #        ${PCL_LIBRARIES}
        #        ${TORCH_LIBRARIES}
        #        ${libLAS_LIBRARIES}
        ${realsense2_LIBRARY}
        ${MRAA_LIBRARIES}
        glib-2.0
        stdc++
        stdc++fs
        m
        ${MPI_CXX_LIBRARIES}
        pistache_shared
        libserv
        ${PROTOBUF_LIBRARY}
        ${TBB_IMPORTED_TARGETS}
        ${InferenceEngine_LIBRARIES}
        ${CERES_LIBRARIES}
        ${ABSL_LIBRARIES}
        ${CARTOGRAPHER_LIBRARIES}
        )

foreach (RC_THREAD_LIB ${RC_THREAD_LIBS})
    message(STATUS "RC_THREAD_LIB:${RC_THREAD_LIB}")
endforeach ()
if (${WITH_LIBREALSENCE2} STREQUAL "ON")
    LIST(APPEND RC_THREAD_LIBS ${realsense2_LIBRARY})
endif ()

if (${WITH_TBB} STREQUAL "ON")
    LIST(APPEND RC_THREAD_LIBS ${TBB_IMPORTED_TARGETS})
endif ()

add_library(rccore SHARED
        ${RC_HEADERS}
        ${RC_SOURCES}
        ${RC_PROTO_SRCS}
        ${RC_PROTO_HDRS}
        )
add_dependencies(rccore proto_dep copy_html copy_haarcascades)
target_link_libraries(rccore ${OpenCV_LIBS}
        ${RC_THREAD_LIBS}
        )



