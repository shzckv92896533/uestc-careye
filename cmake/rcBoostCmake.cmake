message(STATUS "FIND Boost")
find_package(Boost REQUIRED COMPONENTS
        date_time filesystem iostreams python thread system mpi)
set(Boost_USE_STATIC_LIBS        OFF)  # only find static libs
set(Boost_USE_DEBUG_LIBS         OFF) # ignore debug libs and
set(Boost_USE_RELEASE_LIBS       ON)  # only find release libs
message(STATUS "Boost_VERSION ${Boost_VERSION}")
include_directories(${Boost_INCLUDE_DIRS})
message(STATUS "Boost_INCLUDE_DIRS:${Boost_INCLUDE_DIRS}")
link_directories(${Boost_LIBRARY_DIRS})
#Boost_FOUND            - True if headers and requested libraries were found
#Boost_INCLUDE_DIRS     - Boost include directories
#Boost_LIBRARY_DIRS     - Link directories for Boost libraries
#Boost_LIBRARIES        - Boost component libraries to be linked
#Boost_<C>_FOUND        - True if component <C> was found (<C> is upper-case)
#Boost_<C>_LIBRARY      - Libraries to link for component <C> (may include
#target_link_libraries debug/optimized keywords)
#Boost_VERSION          - BOOST_VERSION value from boost/version.hpp
#Boost_LIB_VERSION      - Version string appended to library filenames
#Boost_MAJOR_VERSION    - Boost major version number (X in X.y.z)
#Boost_MINOR_VERSION    - Boost minor version number (Y in x.Y.z)
#Boost_SUBMINOR_VERSION - Boost subminor version number (Z in x.y.Z)
#Boost_LIB_DIAGNOSTIC_DEFINITIONS (Windows)
#- Pass to add_definitions() to have diagnostic
#information about Boost's automatic linking
#displayed during compilation
